# Projet M2L

Ce projet est un site web pour la Maison des Ligues de Lorraine (M2L), développé en React.JS.

## Installation

1. Cloner le repository :

```bash
git clone https://gitlab.com/dodq1G/m2l.git
```

2. Installer les dépendances :

```bash
cd m2l\m2l\
npm install
```

3. Démarrer le serveur de développement :

```bash
npm start
```

## Utilisation

Le site web est accessible à l'adresse `http://localhost:3000` dans votre navigateur web. Vous pouvez naviguer sur le site pour découvrir ses fonctionnalités.

## Contribution

Les contributions sont les bienvenues ! Si vous souhaitez contribuer à ce projet, voici les étapes à suivre :

1. Forker le repository.
2. Déplacez-vous sur une branche pour votre contribution :
```bash
   git checkout main
   ```
3. Faire vos modifications et tester le site web localement.
4. Ajouter et commiter les modifications :
```bash
   git add .
   git commit -m "Description de votre contribution"
   ```
5. Pousser les modifications sur votre fork :
```bash
   git push origin main
   ```
6. Créer une pull request sur le repository original.
