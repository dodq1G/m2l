import React, { useState, useEffect } from "react";
import { Container, Table, Button } from "react-bootstrap";
import axios from "axios";

function Cart() {
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("cart")) || [];
    setCartItems(items);
  }, []);

  const removeItem = (index) => {
    const newItems = [...cartItems];
    const item = newItems[index];

    if (item.count <= 1) {
      newItems.splice(index, 1);
    } else {
      item.count--;
    }

    localStorage.setItem("cart", JSON.stringify(newItems));
    setCartItems(newItems);
    window.location.reload();
  };

  const clearCart = () => {
    localStorage.removeItem("cart");
    setCartItems([]);
  };

  const subTotal = cartItems.reduce((acc, item) => {
    return acc + item.prix * item.count;
  }, 0);

  const handleCartValidation = async () => {
    // Vérifier si la quantité du panier dépasse la quantité indiquée sur les produits
    const isValidCart = cartItems.every((item) => item.count <= item.quantite);

    if (isValidCart) {
      // Récupérer l'utilisateur connecté à partir du localStorage
      const user = JSON.parse(localStorage.getItem("user"));

      // Vérifier si l'utilisateur est connecté
      if (user && user.id) {
        // Envoyer la requête pour ajouter une commande
        try {
          const date = new Date().toISOString().split("T")[0];
          const nbArticle = cartItems.length;
          const total = subTotal;
          const quantite = cartItems.reduce((acc, item) => acc + item.count, 0);
          const idUtilisateur = user.id;

          await axios.post("http://localhost:8000/commande/add", {
            dateCommande: date,
            nbArticle,
            total,
            quantite,
            idUtilisateur,
          });

          alert("Le panier a été validé avec succès !");
          window.location.reload();
          clearCart();
        } catch (error) {
          console.error(error);
        }
      } else {
        alert("Vous devez vous connecter pour valider le panier.");
      }
    } else {
      alert(
        "La quantité sélectionnée dépasse la quantité disponible pour certains produits."
      );
    }
  };

  return (
    <Container>
      <center style={{ marginTop: "1.5rem", marginBottom: "2rem" }}>
        <h1 className="h1-responsive font-weight-bold text-center my-4">
          <u>Panier</u>
        </h1>
      </center>
      <Table striped bordered variant="yellow">
        <thead>
          <tr className="text-danger">
            <th>Référence</th>
            <th>Nom du produit</th>
            <th>Description</th>
            <th>Quantité</th>
            <th>Prix unitaire</th>
            <th>Prix total</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {cartItems.map((item, index) => (
            <tr
              key={index}
              className={index % 2 === 0 ? "bg-warning" : "bg-dark"}
            >
              <td>{item.reference}</td>
              <td>{item.nomProduit}</td>
              <td>{item.description}</td>
              <td>{item.count}</td>
              <td>{item.prix} €</td>
              <td>{item.prix * item.count} €</td>
              <td>
                <a
                  href="#"
                  onClick={() => removeItem(index)}
                  className="text-danger"
                >
                  <i
                    style={{ color: "black" }}
                    className="bi bi-trash-fill"
                  ></i>
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <center>
        <h4>Sous-total : {subTotal} €</h4>
        <div style={{ paddingTop: "1rem" }}>
          <Button variant="danger" onClick={handleCartValidation}>
            Valider le panier
          </Button>
        </div>
      </center>
    </Container>
  );
}

export default Cart;
