import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "../css/Produit.css";

function Produits() {
  const [allprod, setallProd] = useState([]);

  const allproduits = async () => {
    await axios
      .get("http://localhost:8000/produits")
      .then((res) => {
        setallProd(res.data);
      });
  };

  const handleAddToCart = (prod) => {
    let cartItems = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : [];
    let productAlreadyInCart = false;
  
    cartItems.forEach((item) => {
      if (item.reference === prod.reference) {
        productAlreadyInCart = true;
        item.count++;
      }
    });
  
    if (!productAlreadyInCart) {
      cartItems.push({ ...prod, count: 1 });
    }
  
    localStorage.setItem("cart", JSON.stringify(cartItems));
    window.location.reload(); // rafraîchir la page
  };
  

  useEffect(() => {
    allproduits();
  }, []);

  return (
    <div className="container">
      <h2 className="h1-responsive font-weight-bold text-center my-4">
        <u>Nos produits</u>
      </h2>
      <div className="row">
        <div className="col-md-12 mb-4">
          <div className="text-center">
            <button
              className="btn btn-light"
              onClick={() => {
                axios
                  .get("http://localhost:8000/produits/orderByPrix")
                  .then((res) => {
                    setallProd(res.data);
                  });
              }}
            >
              <i className="bi bi-arrow-down-up"></i> Filtrer par prix
            </button>
            &nbsp;
            <button
              className="btn btn-light"
              onClick={() => {
                axios
                  .get("http://localhost:8000/produits/orderByNom")
                  .then((res) => {
                    setallProd(res.data);
                  });
              }}
            >
              <i className="bi bi-sort-alpha-down"></i> Filtrer par nom
            </button>
          </div>
        </div>
      </div>
      <div className="row">
        {allprod.map((prod) => (
          <div className="col-md-4" key={prod.reference}>
            <div className="card mb-4 shadow-sm">
              <div className="card-body text-center">
                <img
                  src={require("../assets/" + prod.nomProduit + ".jpg")}
                  style={{ width: 100, height: 100 }}
                  className="img-fluid"
                  alt={"../assets/" + prod.nomProduit}
                />
                
                <h5 className="card-title">{prod.nomProduit}</h5>
                <h5 className="card-title">Prix: {prod.prix} €</h5>
                <h5 style={{color:"grey", fontSize:"0.9rem"}}>Quantité disponible: {prod.quantite}</h5>
                <div style={{marginTop:"2rem"}}>
                  <button
                    className="btn btn-warning"
                    onClick={() => handleAddToCart(prod)}
                    disabled={prod.quantite === 0} // Griser le bouton lorsque la quantité est à 0
                  >
                    <i className="bi bi-cart-plus"></i> Ajouter au panier
                  </button>
                  <Link
                    to={"/details/" + prod.reference}
                    className="btn btn-danger"
                    style={{ marginLeft: 5 }}
                  >
                    <i className="bi bi-arrow-right-circle"></i> Voir les détails
                  </Link>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Produits;
