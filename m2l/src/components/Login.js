import { useState } from 'react';
import axios from 'axios';
import '../css/login.css';

function Login() {
  const [mail, setEmail] = useState('');
  const [mdp, setMdp] = useState('');

  const handleLogin = async () => {
    try {
      const response = await axios.post('http://localhost:8000/login', { mail, mdp });

      if (response.status === 200) {
        const userData = response.data;
        // stocker les données utilisateur dans le localStorage
        localStorage.setItem('user', JSON.stringify(userData));
        // naviguer vers la page de profil
        window.location.href = '/produits';
      } else {
        const errorText = response.data;
        throw new Error(errorText);
      }
    } catch (error) {
      console.error(error);
      alert("L'addresse mail ou le mot de passe est incorrecte !");
    }

  };

  return (
    <div>
      <h2 className='title'>Login</h2>
      <div className='content'>
        <form>
          <div style={{ width: '30%' }}>
            <div className='form-group'>
              <label htmlFor='mail'>Email</label>
              <input
                type='text'
                className='form-control'
                id='mail'
                placeholder='Enter your email'
                value={mail}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className='form-group'>
              <label htmlFor='mdp'>Mot de passe</label>
              <input
                type='password'
                className='form-control'
                id='mdp'
                placeholder='Password'
                value={mdp}
                onChange={(e) => setMdp(e.target.value)}
              />
            </div>
            <a style={{ fontSize: '0.8rem' }} href='/register'>
              Vous n'avez pas encore de compte ? Cliquez ici
            </a>
            <div className='submit'>
              <button
                type='button'
                className='btn btn-warning'
                onClick={handleLogin}
              >
                S'identifier
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
