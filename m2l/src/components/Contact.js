import '../css/contact.css'
function Contact() {
  return (<div>
    <h2 className='title'>Contact</h2>
    <div className='content'>
      <form>
        <div style={{ width: "30%" }}>
          <div style={{ marginBottom: '1rem' }} class="form-group">
            <label for="subject">Sujet</label>
            <input type="text" class="form-control" id="subject" aria-describedby="subject" placeholder="Sujet" />
          </div>
          <div class="form-group">
            <textarea class="form-control" id="message" placeholder='Ecrire votre message ...' rows="5"></textarea>
          </div>
          <div class='submit'>
            <button type="button" class="btn btn-warning">Envoyer</button>
          </div>
        </div>
      </form>
    </div>
  </div>)
}

export default Contact;