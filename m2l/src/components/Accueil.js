import React from 'react';
import { Container, Row, Col, Image, Button } from 'react-bootstrap';

import sport1 from '../img/imagefootassociation.jpg';
import sport2 from '../img/imagevolleyassociation.jpg';
import sport3 from '../img/imagegolfassociation.jpg';
function Accueil() {
    return (
        <div className="container">
        <Container className="py-5">
          <Row className="mb-4">
            <Col>
              <h1 className="text-center">Bienvenue à l'Association M2L</h1>
              <p className="lead text-center">Promouvoir le sport dans la région</p>
            </Col>
          </Row>
          <Row>
            <Col>
              <Image src={sport1} width="240rem" fluid />
            </Col>
            <Col>
              <Image src={sport2} width="280rem" fluid />
            </Col>
            <Col>
              <Image src={sport3} width="300rem" fluid />
            </Col>
          </Row>
          <Row className="my-5">
            <Col>
              <h2>Nos services</h2>
              <p>L'Association M2L est une organisation à but non-lucratif dédiée à promouvoir le sport dans la région. Depuis sa création, notre association a pour mission de rendre le sport accessible à tous et d'encourager la pratique d'activités physiques pour améliorer la santé et le bien-être de nos concitoyens.
                <br></br><br></br>
                Nous proposons des produits sportifs de qualité à des prix abordables. 
Nous pensons que la promotion de produits sportifs de qualité peut aider à encourager davantage de gens à se 
lancer dans une activité physique, et à améliorer leur performance et leur expérience sportive.
              </p>
            </Col>
          </Row>
        </Container>
        </div>
    )
};

export default Accueil;