import { useParams } from 'react-router-dom';
import React, { useEffect, useState } from "react";
import axios from "axios";
import "../css/Details.css"
import { Container, Card, Button } from "react-bootstrap";

function Details() {
  const { ref } = useParams();
  const [prod, setProd] = useState([])
  const getProduit = async () => {
    await axios.get(`http://localhost:8000/produit/${ref}`)
      .then(res => {
        console.log(res.data)
        setProd(res.data)
      })
  }

  const handleAddToCart = (prod) => {
    let cartItems = JSON.parse(localStorage.getItem("cart")) || [];
    let productAlreadyInCart = false;

    cartItems.forEach((item) => {
      if (item.reference === prod.reference) {
        productAlreadyInCart = true;
        item.count++;
      }
    });

    if (!productAlreadyInCart) {
      cartItems.push({ ...prod, count: 1 });
    }

    localStorage.setItem("cart", JSON.stringify(cartItems));
    window.location.reload(); // rafraîchir la page
  };

  useEffect(() => {
    getProduit()
  }, [])

  return (
    <Container style={{ marginTop: "2rem", display: "flex", justifyContent: "center", alignItems: "center" }}>
      {prod?.map((produit) => (
        <Card style={{ width: "25rem" }} key={produit.reference}>
          <div style={{ margin: "1rem 1rem 1rem 1rem" }}>
            <h5 className="card-title">{produit.nomProduit}</h5>
            <h5 className="mb-2 text-muted">
              Référence: {produit.reference}
            </h5>
            <img src={require("../assets/" + produit.nomProduit + ".jpg")} style={{
              width: 300, height: 300
            }} alt={"../assets/" + produit.nomProduit} />
            <h6><u>Description : </u></h6>
            <p>{produit.description}</p>
            <h6 className="card-title">Prix: {produit.prix} €</h6>
            <h5 style={{ color: "grey", fontSize: "0.9rem" }}>Quantité disponible: {produit.quantite}</h5>
            <div style={{ marginTop: "1rem" }}>
              <button
                key="add-to-cart-button"
                className="btn btn-warning"
                onClick={() => handleAddToCart(prod)}
              >
                <i className="bi bi-cart-plus"></i> Ajouter au panier
              </button>
            </div>
          </div>
        </Card>
      ))}
    </Container>
  );
}

export default Details;
