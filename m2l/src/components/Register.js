import axios from 'axios';
import '../css/register.css';
import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

function Register() {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const [nom, setNom] = useState('');
  const [prenom, setPrenom] = useState('');
  const [email, setEmail] = useState('');
  const [mdp, setMdp] = useState('');
  const [emailExists, setEmailExists] = useState(false); // Nouvelle variable d'état

  useEffect(() => {
    const checkEmailExists = async () => {
      try {
        const response = await axios.get(`http://localhost:8000/user/email/${email}`);
        setEmailExists(response.data.exists);
      } catch (error) {
        console.error(error);
      }
    };

    if (email !== '') {
      checkEmailExists();
    }
  }, [email]);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (emailExists) {
      alert("L'adresse e-mail existe déjà !");
    } else {
      axios.post('http://localhost:8000/user/new', {
        nomUtilisateur: nom,
        mdp: mdp,
        role: 'user',
        prenomUtilisateur: prenom,
        mail: email
      })
        .then((res) => {
          console.log(res);
          toggle(); // Ouvrir la modal uniquement si l'enregistrement a réussi
        })
        .catch((error) => console.error(error));
    }
  };

  return (
    <div>
      <h2 className='title'>Register</h2>
      <div className='content'>
        <form onSubmit={handleSubmit}>
          <div style={{ display: 'flex' }}>
            <div className='form-group'>
              <label htmlFor='nom'>Nom</label>
              <input
                type='text'
                className='form-control'
                id='nom'
                placeholder='Nom'
                value={nom}
                onChange={(event) => setNom(event.target.value)}
              />
            </div>
            <div style={{ paddingLeft: '31px' }} className='form-group'>
              <label htmlFor='prenom'>Prénom</label>
              <input
                type='text'
                className='form-control'
                id='prenom'
                placeholder='Prénom'
                value={prenom}
                onChange={(event) => setPrenom(event.target.value)}
              />
            </div>
          </div>
          <div style={{ width: '30%' }}>
            <div className='form-group'>
              <label htmlFor='email'>Email address</label>
              <input
                type='email'
                className='form-control'
                id='email'
                aria-describedby='email'
                placeholder='Entrer votre email'
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
              <small
                style={{ fontSize: '0.8rem' }}
                id='infos'
                className='form-text text-muted'
              >
                Nous ne partagerons jamais votre adresse mail.
              </small>
            </div>
            {emailExists && ( // Affichage de l'alerte si l'e-mail existe déjà
              <div className='alert alert-danger' role='alert'>
                L'adresse e-mail existe déjà !
              </div>
            )}
            <div className='form-group'>
              <label htmlFor='mdp'>Mot de passe</label>
              <input
                type='password'
                className='form-control'
                id='mdp'
                placeholder='Password'
                value={mdp}
                onChange={(event) => setMdp(event.target.value)}
              />
              <a style={{ fontSize: '0.8rem' }} href='/Login'>Vous avez déjà un compte ? Cliquez ici</a>
            </div>
            <div className='submit'>
              <button type='submit' className='btn btn-warning'>S'enregistrer</button>
              <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Inscription réussie !</ModalHeader>
                <ModalBody>
                  Votre compte a bien été enregistré dans la base de données.
                </ModalBody>
                <ModalFooter>
                  <Link to="/login"><Button color="warning">Fermer</Button></Link>
                </ModalFooter>
              </Modal>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Register;