import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Table } from 'react-bootstrap';

function Users() {
  const [user, setUser] = useState({});
  const [commandes, setCommandes] = useState([]);

  useEffect(() => {
    // Récupérer les informations de l'utilisateur à partir du localStorage
    const storedUser = localStorage.getItem('user');
    if (storedUser) {
      setUser(JSON.parse(storedUser));
      fetchCommandes(JSON.parse(storedUser).id);
    }
  }, []);

  const fetchCommandes = (userId) => {
    fetch(`http://localhost:8000/commande/history/${userId}`)
      .then(response => response.json())
      .then(data => {
        setCommandes(data);
      })
      .catch(error => {
        console.error('Erreur lors de la récupération de l\'historique des commandes:', error);
      });
  };

  const handleEmailChange = (e) => {
    setUser({ ...user, mail: e.target.value });
  };

  const handlePasswordChange = (e) => {
    setUser({ ...user, mdp: e.target.value });
  };

  const handleSaveChanges = () => {
    // Envoyer les modifications à la base de données
    fetch(`http://localhost:8000/user/${user.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user),
    })
      .then(response => response.text())
      .then(data => {
        alert(data);
      })
      .catch(error => {
        console.error('Erreur lors de la modification de l\'utilisateur:', error);
        alert('Erreur lors de la modification de l\'utilisateur.');
      });

    localStorage.setItem('user', JSON.stringify(user));
    alert('Les changements ont été enregistrés.');
  };


  return (
    <Container className="py-5">
      <Row>
        <Col>
          <h2 className="h1-responsive font-weight-bold text-center my-4">
            <u>Mon profil</u>
          </h2>
          <Form>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">Nom</Form.Label>
              <Col sm="10">
                <Form.Control type="text" value={user.nomUtilisateur} readOnly />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">Prénom</Form.Label>
              <Col sm="10">
                <Form.Control type="text" value={user.prenomUtilisateur} readOnly />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">Email</Form.Label>
              <Col sm="8">
                <Form.Control type="email" value={user.mail} onChange={handleEmailChange} />
              </Col>
              <Col sm="2">
                <Button variant="outline-warning">Modifier</Button>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Form.Label column sm="2">Mot de passe</Form.Label>
              <Col sm="8">
                <Form.Control type="password" value="**********" onChange={handlePasswordChange} />
              </Col>
              <Col sm="2">
                <Button variant="outline-warning">Modifier</Button>
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3">
              <Col sm={{ offset: 2 }}>
                <Button variant="danger" onClick={handleSaveChanges}>Enregistrer les changements</Button>
              </Col>
            </Form.Group>
          </Form>
        </Col>
      </Row>

      <Row>
        <Col>
          <h2 className="h1-responsive font-weight-bold text-center my-4">
            <u>Historique des commandes</u>
          </h2>
          <Table striped bordered variant="yellow">
            <thead>
              <tr className="text-danger">
                <th>Date de commande</th>
                <th>Nombre d'articles</th>
                <th>Total</th>
                <th>Quantité</th>
              </tr>
            </thead>
            <tbody>
              {commandes.map((commande, index) => (
                <tr
                  key={index}
                  className={index % 2 === 0 ? 'bg-warning' : 'bg-dark'}
                >
                  <td>{new Date(commande.dateCommande).toLocaleDateString()}</td>
                  <td>{commande.nbArticle}</td>
                  <td>{commande.total} €</td>
                  <td>{commande.quantite}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}

export default Users;
