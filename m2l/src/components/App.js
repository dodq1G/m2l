import "../css/App.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import '@fortawesome/fontawesome-svg-core/styles.css';
import Accueil from "./Accueil";
import Produits from "./Produits";
import Login from "./Login";
import Register from "./Register";
import Users from "./Users";
import Contact from "./Contact";
import Details from "./Details";
import Admin from "./Admin";
import Cart from "./Cart";
import { useEffect, useState } from "react";

function App() {
  const [userType, setUserType] = useState();
  const [userName, setUserName] = useState();

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      setUserType(user.role);
      setUserName(user.nomUtilisateur + ' ' + user.prenomUtilisateur);
    }
  }, [])

  return (
    <Router>
      <div>
        <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "white" }}>
          <Link style={{ paddingLeft: "0.8rem" }} className="navbar-brand" to="/">
            <img style={{ width: "8rem", height: "6rem" }} src={require("../assets/M2L.png")} alt="Logo M2L" />
          </Link>
          <button className="navbar-toggler" type="button" onClick={() => document.querySelector(".navbar-collapse").classList.toggle("show")}>
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/">Accueil</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/produits">Produits</Link>
              </li>
              {userType === "admin" && (
                <li className="nav-item">
                  <Link className="nav-link" to="/admin">Admin</Link>
                </li>
              ) || userType === "adminproduit" && (
                <li className="nav-item">
                  <Link className="nav-link" to="/admin">Admin</Link>
                </li>
              )}
              <li className="nav-item">
                <Link className="nav-link" to="/contact">Contact</Link>
              </li>
            </ul>
          </div>
          <div style={{ marginRight: "50px" }}>
            {userType === undefined && (
              <li className="nav-item">
                <Link className="nav-link" to="/login">Login & Register</Link>
              </li>
            )}
            {userType !== undefined && (
              <li className="nav-item dropdown ml-auto">
                <a className="nav-link dropdown-toggle" href="#" role="button" onClick={() => document.querySelector("#navbarDropdown").classList.toggle("show")}>
                  <i className="bi bi-person-circle" style={{ fontSize: "1.5rem", marginRight: "0.5rem" }}></i>
                  {userName}
                </a>
                <div className="cart-icon">
                  <Link to="/cart">
                    <i className="bi bi-cart" style={{ fontSize: "1.5rem", marginRight: "0.5rem" }}></i>
                    {localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")).length : 0}
                  </Link>
                </div>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" id="navbarDropdown">
                  <Link className="dropdown-item" to="/users">Mon profil</Link>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#" onClick={() => { localStorage.removeItem('user'); window.location.href = "/"; }}>Se déconnecter</a>
                </div>
              </li>
            )}

          </div>
        </nav>
        <Routes>
          <Route path="/" element={<Accueil />} />
          <Route path="/produits" element={<Produits />} />
          <Route path="/users" element={<Users />} />
          <Route path="/admin" element={<Admin />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/details" element={<Details />} />
          <Route path="/details/:ref" element={<Details />} />
          <Route path="/cart" element={<Cart/>}></Route>
        </Routes>
      </div>
    </Router>
  );


}

export default App;