import React, { useEffect, useState } from "react";
import "../css/admin.css";
import axios from "axios";
import { Button, Modal } from "react-bootstrap";

function Admin() {
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));

    if (user && user.role === "admin") {
      setIsAdmin(true);
    } else {
      setIsAdmin(false);
    }

    if (
      (user && user.role === "admin") ||
      (user && user.role === "adminproduit")
    ) {
    } else {
      window.location.replace("/");
    }

    allUsers();
    allproduits();
  }, []);

  const [isAdmin, setIsAdmin] = useState(false);
  const [allUser, setAllUser] = useState([]);
  const [allprod, setallProd] = useState([]);
  const [selectedProd, setSelectedProd] = useState(null);
  const [showAddProd, setShowAddProd] = useState(false);
  const [newProd, setNewProd] = useState({
    reference: "",
    nomProduit: "",
    description: "",
    prix: 0,
    quantite: 0,
  });

  const [showEditUserModal, setShowEditUserModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);

  const handleUserInputChange = (event, setter) => {
    setter({ ...selectedUser, [event.target.name]: event.target.value });
  };

  const updateUser = async () => {
    if (selectedUser) {
      await axios
        .put(`http://localhost:8000/user/${selectedUser.id}`, selectedUser)
        .then((res) => {
          setShowEditUserModal(false);
          setSelectedUser(null);
          allUsers();
        });
    }
  };

  const [showEditModal, setShowEditModal] = useState(false);
  const [editedProd, setEditedProd] = useState(null);

  const updateProduct = async () => {
    if (editedProd) {
      await axios
        .put(
          `http://localhost:8000/produit/${editedProd.reference}`,
          editedProd
        )
        .then((res) => {
          setShowEditModal(false);
          setEditedProd(null);
          allproduits();
        });
    }
  };

  const handleInputChange = (event, setter) => {
    setter({ ...editedProd, [event.target.name]: event.target.value });
  };

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const handleDeleteProd = (reference) => {
    setSelectedProd(reference);
    setShowDeleteModal(true);
  };

  const allUsers = async () => {
    await axios.get("http://localhost:8000/user").then((res) => {
      setAllUser(res.data);
    });
  };

  const deleteUser = async (id) => {
    await axios
      .delete(`http://localhost:8000/user/${id}`)
      .then(console.log("deleted successfully!"));
  };

  const allproduits = async () => {
    try {
      const response = await axios.get("http://localhost:8000/produits");
      setallProd(response.data);
    } catch (error) {
      console.error("Erreur lors de la récupération des produits :", error);
    }
  };

  const deleteProd = async (reference) => {
    try {
      await axios.delete(`http://localhost:8000/produit/${reference}`);
      setShowDeleteModal(false);
      setSelectedProd(null);
      await allproduits();
    } catch (error) {
      console.error("Erreur lors de la suppression du produit :", error);
    }
  };

  const addProduct = async () => {
    try {
      await axios.post(`http://localhost:8000/produit/add`, newProd);
      setShowAddProd(false);
      await allproduits();
    } catch (err) {
      console.error("Erreur lors de l'ajout du produit :", err);
    }
  };

  return (
    <div>
      <h2 class="h1-responsive font-weight-bold text-center my-4">
        <u>Admin</u>
      </h2>
      {isAdmin && (
        <>
          <h5 class="text-center mt-5 mb-3">Utilisateurs</h5>
          <div class="px-5">
            <table class="table table-striped table-yellow table-bordered text-center">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">NOM</th>
                  <th scope="col">Prénom</th>
                  <th scope="col">Email</th>
                  <th scope="col">Role</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {allUser.map((user) => (
                  <tr>
                    <th scope="row">{user.id}</th>
                    <td>{user.nomUtilisateur}</td>
                    <td>{user.prenomUtilisateur}</td>
                    <td>{user.mail}</td>
                    <td>{user.role}</td>
                    <td>
                      <a
                        href="#"
                        onClick={() => {
                          setSelectedUser(user);
                          setShowEditUserModal(true);
                        }}
                      >
                        <i className="bi bi-pencil-square"></i>
                      </a>
                      <a href="#" onClick={() => deleteUser(user.id)}>
                        <i className="bi bi-trash-fill"></i>
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </>
      )}
      <h5 class="text-center mt-5 mb-3">Produits</h5>
      <div className="px-5">
        <div style={{ marginBottom: "1rem", textAlign: "center" }}>
          <Button variant="danger" onClick={() => setShowAddProd(true)}>
            Ajouter un produit
          </Button>
        </div>
        <table class="table table-striped table-yellow table-bordered text-center">
          <thead>
            <tr>
              <th scope="col-1">Référence</th>
              <th scope="col">Titre</th>
              <th scope="col">Description</th>
              <th scope="col">Prix</th>
              <th scope="col">Stock</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {allprod.map((produit) => (
              <tr>
                <th scope="row">{produit.reference}</th>
                <td>{produit.nomProduit}</td>
                <td>{produit.description}</td>
                <td>{produit.prix} €</td>
                <td>{produit.quantite}</td>
                <td>
                  <a
                    href="#"
                    onClick={() => {
                      setEditedProd(produit);
                      setShowEditModal(true);
                    }}
                  >
                    <i className="bi bi-pencil-square"></i>
                  </a>
                  <a
                    href="#"
                    onClick={() => handleDeleteProd(produit.reference)}
                  >
                    <i className="bi bi-trash-fill"></i>
                  </a>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <Modal show={showAddProd} onHide={() => setShowAddProd(false)} centered>
        <Modal.Header closeButton>
          <Modal.Title>Ajouter un produit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="mb-3">
              <label htmlFor="reference" className="form-label">
                Référence
              </label>
              <input
                type="text"
                className="form-control"
                id="reference"
                value={newProd.reference}
                onChange={(event) =>
                  setNewProd({ ...newProd, reference: event.target.value })
                }
              />
            </div>
            <div className="mb-3">
              <label htmlFor="titre" className="form-label">
                Titre
              </label>
              <input
                type="text"
                className="form-control"
                id="titre"
                value={newProd.nomProduit}
                onChange={(event) =>
                  setNewProd({ ...newProd, nomProduit: event.target.value })
                }
              />
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">
                Description
              </label>
              <textarea
                className="form-control"
                id="description"
                rows="3"
                value={newProd.description}
                onChange={(event) =>
                  setNewProd({ ...newProd, description: event.target.value })
                }
              ></textarea>
            </div>
            <div className="mb-3">
              <label htmlFor="prix" className="form-label">
                Prix
              </label>
              <input
                type="number"
                className="form-control"
                id="prix"
                min="0"
                value={newProd.prix}
                onChange={(event) =>
                  setNewProd({ ...newProd, prix: event.target.value })
                }
              />
            </div>
            <div className="mb-3">
              <label htmlFor="stock" className="form-label">
                Stock
              </label>
              <input
                type="number"
                className="form-control"
                id="stock"
                min="0"
                value={newProd.quantite}
                onChange={(event) =>
                  setNewProd({ ...newProd, quantite: event.target.value })
                }
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowAddProd(false)}>
            Annuler
          </Button>
          <Button variant="danger" onClick={addProduct}>
            Ajouter
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showDeleteModal}
        onHide={() => setShowDeleteModal(false)}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Supprimer le produit</Modal.Title>
        </Modal.Header>
        <Modal.Body>Êtes-vous sûr de vouloir supprimer ce produit ?</Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowDeleteModal(false)}>
            Annuler
          </Button>
          <Button variant="danger" onClick={() => deleteProd(selectedProd)}>
            Supprimer
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showEditModal}
        onHide={() => setShowEditModal(false)}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Modifier le produit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {editedProd && (
            <form>
              <div className="mb-3">
                <label htmlFor="reference" className="form-label">
                  Référence
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="reference"
                  name="reference"
                  value={editedProd.reference}
                  onChange={(event) => handleInputChange(event, setEditedProd)}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="titre" className="form-label">
                  Titre
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="titre"
                  name="nomProduit"
                  value={editedProd.nomProduit}
                  onChange={(event) => handleInputChange(event, setEditedProd)}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">
                  Description
                </label>
                <textarea
                  className="form-control"
                  id="description"
                  name="description"
                  rows="3"
                  value={editedProd.description}
                  onChange={(event) => handleInputChange(event, setEditedProd)}
                ></textarea>
              </div>
              <div className="mb-3">
                <label htmlFor="prix" className="form-label">
                  Prix
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="prix"
                  name="prix"
                  min="0"
                  value={editedProd.prix}
                  onChange={(event) => handleInputChange(event, setEditedProd)}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="stock" className="form-label">
                  Stock
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="stock"
                  name="quantite"
                  min="0"
                  value={editedProd.quantite}
                  onChange={(event) => handleInputChange(event, setEditedProd)}
                />
              </div>
            </form>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowEditModal(false)}>
            Annuler
          </Button>
          <Button variant="danger" onClick={updateProduct}>
            Modifier
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showEditUserModal}
        onHide={() => setShowEditUserModal(false)}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Modifier l'utilisateur</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {selectedUser && (
            <form>
              <div className="mb-3">
                <label htmlFor="nomUtilisateur" className="form-label">
                  Nom
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="nomUtilisateur"
                  name="nomUtilisateur"
                  value={selectedUser.nomUtilisateur}
                  onChange={(event) =>
                    handleUserInputChange(event, setSelectedUser)
                  }
                />
              </div>
              <div className="mb-3">
                <label htmlFor="prenomUtilisateur" className="form-label">
                  Prénom
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="prenomUtilisateur"
                  name="prenomUtilisateur"
                  value={selectedUser.prenomUtilisateur}
                  onChange={(event) =>
                    handleUserInputChange(event, setSelectedUser)
                  }
                />
              </div>
              <div className="mb-3">
                <label htmlFor="mail" className="form-label">
                  Email
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="mail"
                  name="mail"
                  value={selectedUser.mail}
                  onChange={(event) =>
                    handleUserInputChange(event, setSelectedUser)
                  }
                />
              </div>
              <div className="mb-3">
                <label htmlFor="role" className="form-label">
                  Rôle
                </label>
                <select
                  className="form-select"
                  id="role"
                  name="role"
                  value={selectedUser.role}
                  onChange={(event) =>
                    handleUserInputChange(event, setSelectedUser)
                  }
                >
                  <option value="user">Utilisateur</option>
                  <option value="admin">Administrateur</option>
                  <option value="adminproduit">Administrateur produit</option>
                </select>
              </div>
            </form>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="warning" onClick={() => setShowEditUserModal(false)}>
            Annuler
          </Button>
          <Button variant="danger" onClick={updateUser}>
            Modifier
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default Admin;
